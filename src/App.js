import React, { Component } from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';
import { getCrypto, getChartData, changeCurrency, setCrypto } from './store/middleware';

import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import Popup from './components/Popup/Popup';
import CryptoHeader from './components/CryptoHeader/CryptoHeader';
import Info from './components/Info/Info';
import ChartNavigation from './components/ChartNavigation/ChartNavigation';

import './App.scss';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            activePopup: false,
        }
    }

    componentDidMount() {
        const { getCrypto } = this.props;
        getCrypto('bitcoin');
    }

    changeCurrency = event => {
        const { id, changeCurrency, getChartData, currency, startDate, endDate } = this.props;
        changeCurrency(event.value);

        getChartData(id, currency, startDate, endDate);
    }

    togglePopup = () => {
        this.setState({ activePopup: !this.state.activePopup });
    }

    changeCrypto = crypto => {
        const { setCrypto, getCrypto } = this.props;

        setCrypto(crypto);
        getCrypto(crypto);
    }

    render() {
        const { crypto, loading, currency } = this.props;
        const { activePopup } = this.state;
        let info = null,
            content = null;

        

        if(crypto) {
            info = <div className="content">
                        <CryptoHeader name={ crypto.name } image={ crypto.image.small } currency={ currency } price={ crypto.market_data.current_price[currency] } changePrice={ crypto.market_data.price_change_24h_in_currency[currency] } buttonName={`Buy ${ crypto.id }`} activePopup={ this.togglePopup }/>
                        <Info  marketCap={ crypto.market_data.market_cap[currency] } volume={ crypto.market_data.total_volume[currency] } supply={ crypto.market_data.circulating_supply } totalSupply={ crypto.market_data.total_supply }  />
                    </div>;
        }

        if(loading) {
            content = <h1>Loading</h1>;
        } else {
            content = <div className="content">
                        { crypto ? <Header changeCurrency={ this.changeCurrency } currentPrices={ crypto.market_data.current_price } changeCrypto={ this.changeCrypto } currency={ currency }/> : null }
                        { activePopup ? <Popup closePopup={ this.togglePopup } /> : null}
                        { info }
                        <ChartNavigation/>
                        { crypto ? <Footer date={ crypto.market_data.last_updated } changeCrypto={ this.changeCrypto } /> : null }
                    </div>;
        }
        
        

        return (
            <Router>
                <div className={`App ${ activePopup ? 'overflow-hidden' : '' }`}>
                    <Redirect strict from="/" to="/bitcoin" />
                    <Route exact path="/bitcoin">
                        { content }
                    </Route>
                    <Route exact path="/ethereum">
                        { content }
                    </Route>
                    <Route exact path="/eos">
                        { content }
                    </Route>
                </div>
            </Router>
        );
    }
}

const mapStateToProps = state => ({
    id: state.crypto.id,
    crypto: state.crypto.crypto,
    loading: state.crypto.loading,
    chartData: state.crypto.chartData,
    currency: state.crypto.currency,
    error: state.crypto.error,
    startDate: state.crypto.chartStartDate,
    endDate: state.crypto.chartEndDate,
})

const mapDispatchToProps = dispatch => ({
    getCrypto: id => dispatch(getCrypto(id)),
    getChartData: (id, currency, from, to) => dispatch(getChartData(id, currency, from, to)),
    changeCurrency: currency => dispatch(changeCurrency(currency)),
    setCrypto: crypto => dispatch(setCrypto(crypto)),
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)( App );