import React from 'react';
import ReactDOM from 'react-dom';
import { applyMiddleware, createStore, combineReducers, compose } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { cryptoReducer } from './store/reducer';

const rootReducer = combineReducers({
    crypto: cryptoReducer,
});

const initialState = {};

const store = createStore(
                rootReducer, 
                initialState , 
                compose( 
                    applyMiddleware(thunk),
                    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
                )
            );


ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));

serviceWorker.unregister();
