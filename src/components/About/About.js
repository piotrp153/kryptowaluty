import React from 'react';

import './About.scss';

const about = props => {
    const { description } = props;

    return (
        <div className="about">
            <p dangerouslySetInnerHTML={{__html: description}} />
        </div>
    )
}

export default about;