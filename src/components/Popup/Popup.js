import React from 'react';

import './Popup.scss';

const popup = props => {
    const { closePopup } = props;
    
    return (
        <div className="popup">
            <div className="popup__box">
                <h2>Comming soon</h2>
                <div className="close" onClick={ closePopup }></div>
            </div>
        </div>
    )
}

export default popup;