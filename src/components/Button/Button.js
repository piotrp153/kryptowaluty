import React from 'react';

import './Button.scss';

const button = props => {
    const { className, name, value, onClick } = props;
    return (
        <button className={`button ${ className ? className : ''  } `} value={ value } onClick={ onClick } >{ name }</button>
    )
}

export default button;