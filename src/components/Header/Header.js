import React from 'react';
import Select from 'react-select';
import { Link } from 'react-router-dom';

import './Header.scss';

import logo  from '../../assets/Logo.png';

const header = props => {
    const { changeCurrency, currentPrices, changeCrypto } = props;

    let currencyOptions = [],
        idOfSelected;


    currencyOptions = Object.keys(currentPrices).map((key, index) => {
        const { currency } = props;

        if(key === currency) {
            idOfSelected = index;
        }

        return { value: key, label: key };
    });


    return (
        <div className="container">
            <div className="header">
                <Link to="/">
                    <img src={ logo } alt="" className="header__logo"/>
                </Link>
                <div className="header__menu">
                    <Link to="/bitcoin" onClick={ () => changeCrypto('bitcoin') }>Bitcoin</Link>
                    <Link to="/ethereum" onClick={ () => changeCrypto('ethereum') }>Ethereum</Link>
                    <Link to="/eos" onClick={ () => changeCrypto('eos') }>EOS</Link>
                </div>
                <Select className="header__select" defaultValue={currencyOptions[idOfSelected]} options={ currencyOptions } onChange={ changeCurrency }/>
            </div>
        </div>
    )
}

export default header;