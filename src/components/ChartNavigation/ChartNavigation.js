import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getChartData, getChartDates } from '../../store/middleware';

import Button from '../Button/Button';
import Chart from '../Chart/Chart';
import About from '../About/About';

import './ChartNavigation.scss';

class ChartNavigation extends Component {
    constructor(props) {
        super(props);

        this.state = {
            displayInformation: 'chart',
            chartPeriod: 604800,
            chartDateStart: undefined,
            chartDateEnd: undefined,
        }
    }

    componentDidMount() {
        const { id, getChartData, getChartDates, currency } = this.props;

        let now = Math.round((new Date()).getTime() / 1000);
        let previousDate = Math.round((new Date( now - 604800 )).getTime());

        getChartDates(previousDate, now);
        
        this.setState({ chartDateStart: previousDate, chartDateEnd: now });
        getChartData(id, currency, previousDate, now);

    }

    changeDate = (event) => {
        const { id, getChartData, getChartDates, currency } = this.props;

        let period = 86400;
        const value = event.target.value;
        
        if(value === 'day') {
            period = 86400;
        } else if (value === 'fourHours') {
            period = 14400;
        } else if (value === 'twoHours') {
            period = 7200;
        } else if (value === 'fifteenMinutes') {
            period = 900;
        } else {
            period = 604800;
        }

        this.setState({ chartPeriod: value });
        
        let now = Math.round((new Date()).getTime() / 1000);
        let previousDate = Math.round((new Date( now - period )).getTime());

        this.setState({ chartPeriod: period });

        getChartDates(previousDate, now);
        
        getChartData(id, currency, previousDate, now);
    }

    changeInformation = (event) => {
        const value = event.target.value;
        this.setState({ displayInformation: value });
    }

    render() {
        const { crypto, chartData } = this.props;
        const { displayInformation, chartPeriod } = this.state;

        let information = null;

        if(chartData && displayInformation === 'chart'){
            information = <Chart data={ chartData }/>;
        } else if (crypto && displayInformation === 'about') {
            information = <About description={ crypto.description.en } />;
        }

        return (
            <div className="chart-navigation">
                <div className="container chart-navigation__buttons">
                    <div className="buttons__item">
                        <Button className={ displayInformation === 'chart' ? 'active' : null } name="Charts" value="chart" onClick={ this.changeInformation } />
                        <Button className={ displayInformation === 'about' ? 'active' : null } name="About" value="about" onClick={ this.changeInformation } />
                    </div>
                    <div className="buttons__item">
                        <Button className={ chartPeriod === 604800 ? 'active' : null } name="1W" value="week" onClick={ this.changeDate } /> 
                        <Button className={ chartPeriod === 86400 ? 'active' : null } name="1D" value="day" onClick={ this.changeDate } />
                        <Button className={ chartPeriod === 14400 ? 'active' : null } name="4H" value="fourHours" onClick={ this.changeDate } />
                        <Button className={ chartPeriod === 7200 ? 'active' : null } name="2H" value="twoHours" onClick={ this.changeDate } />
                        <Button className={ chartPeriod === 900 ? 'active' : null } name="15M" value="fifteenMinutes" onClick={ this.changeDate } />
                    </div>
                </div>
                <div className="container">
                    <div className="chart-navigation__info">
                        { information }
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    id: state.crypto.id,
    crypto: state.crypto.crypto,
    chartData: state.crypto.chartData,
    currency: state.crypto.currency,
    startDate: state.crypto.chartStartDate,
    endDate: state.crypto.chartEndDate,
})

const mapDispatchToProps = dispatch => ({
    getChartData: (id, currency, from, to) => dispatch(getChartData(id, currency, from, to)),
    getChartDates: (startDate, endDate) => dispatch(getChartDates(startDate, endDate)),
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)( ChartNavigation );