import React, { Component } from 'react';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";

import './Chart.scss';

am4core.useTheme(am4themes_animated);

class Chart extends Component {

    componentDidMount() {
        this.chartDisplay();
    }

    componentDidUpdate() {
        this.chartDisplay();
    }

    chartDisplay = () => {
        const chartData = this.props.data;

        let chart = am4core.create("chartdiv", am4charts.XYChart),
            data = [];

        for (let i = 0; i < chartData.market_caps.length; i++) {
            data.push({ date1: new Date(chartData.market_caps[i][0]), value1: chartData.market_caps[i][1] });
        }
        for (let j = 0; j < chartData.prices.length; j++) {
            data.push({ date2: new Date(chartData.prices[j][0]), value2: chartData.prices[j][1] });
        }

        chart.data = data;

        let dateAxis = chart.xAxes.push(new am4charts.DateAxis());
        dateAxis.renderer.grid.template.location = 0;
        dateAxis.renderer.labels.template.fill = am4core.color("#e59165");

        let dateAxis2 = chart.xAxes.push(new am4charts.DateAxis());
        dateAxis2.renderer.grid.template.location = 0;
        dateAxis2.renderer.labels.template.fill = am4core.color("#dfcc64");

        let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis.tooltip.disabled = true;
        valueAxis.renderer.labels.template.fill = am4core.color("#e59165");

        valueAxis.renderer.minWidth = 60;

        let valueAxis2 = chart.yAxes.push(new am4charts.ValueAxis());
        valueAxis2.tooltip.disabled = true;
        valueAxis2.renderer.grid.template.strokeDasharray = "2,3";
        valueAxis2.renderer.labels.template.fill = am4core.color("#dfcc64");
        valueAxis2.renderer.minWidth = 60;

        let series = chart.series.push(new am4charts.LineSeries());
        series.name = "Market Cap";
        series.dataFields.dateX = "date1";
        series.dataFields.valueY = "value1";
        series.tooltipText = "{valueY.value}";
        series.fill = am4core.color("#475FF2");
        series.stroke = am4core.color("#475FF2");

        let series2 = chart.series.push(new am4charts.LineSeries());
        series2.name = "Price";
        series2.dataFields.dateX = "date2";
        series2.dataFields.valueY = "value2";
        series2.yAxis = valueAxis2;
        series2.xAxis = dateAxis2;
        series2.tooltipText = "{valueY.value}";
        series2.fill = am4core.color("#F89318");
        series2.stroke = am4core.color("#F89318");

        chart.cursor = new am4charts.XYCursor();
        chart.cursor.xAxis = dateAxis2;

        let scrollbarX = new am4charts.XYChartScrollbar();
        scrollbarX.series.push(series);
        chart.scrollbarX = scrollbarX;

        chart.legend = new am4charts.Legend();
        chart.legend.parent = chart.plotContainer;
        chart.legend.zIndex = 100;

        valueAxis2.renderer.grid.template.strokeOpacity = 0.07;
        dateAxis2.renderer.grid.template.strokeOpacity = 0.07;
        dateAxis.renderer.grid.template.strokeOpacity = 0.07;
        valueAxis.renderer.grid.template.strokeOpacity = 0.07;
    }

    
    componentWillUnmount() {
        if (this.chart) {
            this.chart.dispose();
        }
    }

    render() {
        return (
            <div className="chart">
                <div id="chartdiv" style={{ width: "100%", height: "500px" }} ></div>
            </div>
        );
    }
}

export default Chart
