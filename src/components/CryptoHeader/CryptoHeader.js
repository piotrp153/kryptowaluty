import React from 'react';

import Button from '../Button/Button';

import './CryptoHeader.scss';

const cryptoHeader = props => {
    const { name, image, currency, price, changePrice, buttonName, activePopup } = props;

    return (
        <div className="container">
            <div className="crypto-header">
                <div className="crypto-header__logo">
                    <img src={ image } alt=""/>
                    <h1 className="logo__name">{ name }</h1>
                </div>
                <div className="crypto-header__buy">
                    <div className="buy__price">
                        <p className="price__normal">{ price } <span>{ currency }</span></p>
                        <p className="price__change">{ changePrice } <span>{ currency }</span></p> 
                    </div>
                    <Button className='active' name={ buttonName } value="popup" onClick={ activePopup } />
                </div>
            </div>
        </div>
    )
}

export default cryptoHeader;