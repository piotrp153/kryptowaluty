import React from 'react';

import './Info.scss';

const info = props => {
    const { marketCap, volume, supply, totalSupply } = props;

    return (
        <div className="container">
            <div className="info">
                <div className="info__item">
                    <p className="item__title">Market Cap</p>
                    <p className="item__value">
                        { marketCap }
                    </p>
                </div>
                <div className="info__item">
                <p className="item__title">Volume (24h)</p>
                    <p className="item__value">
                        { volume }
                    </p>
                </div>
                <div className="info__item">
                    <p className="item__title">Circulating Supply</p>
                    <p className="item__value">
                        { supply }
                    </p>
                </div>
                <div className="info__item">
                    <p className="item__title">Max Supply</p>
                    <p className="item__value">
                        { totalSupply }
                    </p>
                </div>
            </div>
        </div>
    )
}

export default info;