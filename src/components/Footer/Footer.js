import React from 'react';
import { Link } from 'react-router-dom';

import './Footer.scss';

const footer = props => {
    const { date, changeCrypto } = props;
    
    return (
        <div className="footer">
            <div className="container footer__holder">
                <p className="footer__update">Last update: { date }</p> 
                <p className="footer__copy" >Zadanie rekrutacyjne Try Codnet</p>
                <div className="footer__menu">
                    <Link to="/bitcoin" onClick={ () => changeCrypto('bitcoin') }>Bitcoin</Link>
                    <Link to="/ethereum" onClick={ () => changeCrypto('ethereum') }>Ethereum</Link>
                    <Link to="/eos" onClick={ () => changeCrypto('eos') }>EOS</Link>
                </div>
            </div>
        </div>
    )
}

export default footer;