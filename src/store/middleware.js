import { getCryptoSuccess, getError, getChartDataSuccess, changeCurrencyAction, changeChartDates, changeCryptoState } from './actions';
import axios from 'axios';

export const getCrypto = id => async dispatch => {
    try {
        const res = await axios.get('https://api.coingecko.com/api/v3/coins/'+ id);
        dispatch(getCryptoSuccess(res.data));

    } catch (error) {
        dispatch(getError(error));
    }
};

export const getChartData = (id, currency, from, to) => async dispatch => {
    try {
        const res = await axios.get('https://api.coingecko.com/api/v3/coins/'+ id +'/market_chart/range?vs_currency='+ currency +'&from='+ from +'&to='+ to);
        dispatch(getChartDataSuccess(res.data));

    } catch (error) {
        dispatch(getError(error));
    }
};

export const changeCurrency = currency => dispatch => {
    dispatch(changeCurrencyAction(currency));
};

export const getChartDates = (startDate, endDate) => dispatch => {
    dispatch(changeChartDates(startDate, endDate));
}

export const setCrypto = (crypto) => dispatch => {
    dispatch(changeCryptoState(crypto));
}