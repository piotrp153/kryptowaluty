import {GET_CRYPTO_SUCCESS, GET_CHART_DATA_SUCCESS, GET_ERROR, CHANGE_CURRENCY, CHANGE_CHART_DATES, CHANGE_CRYPTO_STATE } from './actions';

export const initialState = {
    id: 'bitcoin',
    currency: 'usd',
    crypto: undefined,
    chartData: undefined,
    chartStartDate: undefined,
    chartEndDate: undefined,
    loading: true,
    error: null
}

export function cryptoReducer(state = initialState, action) {
    switch(action.type) {
        case GET_CRYPTO_SUCCESS:
            return {
                ...state,
                loading: false,
                crypto: action.payload
            }
        case GET_CHART_DATA_SUCCESS:
            return {
                ...state,
                loading: false,
                chartData: action.payload
            }
        case GET_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payload,
            }
        case CHANGE_CURRENCY: 
            return {
                ...state,
                currency: action.payload,
            }
        case CHANGE_CHART_DATES: 
            return {
                ...state,
                chartStartDate: action.payload.startDate,
                chartEndDate: action.payload.endDate,
            }
        case CHANGE_CRYPTO_STATE: 
            return {
                ...state,
                id: action.payload
            }
        default: 
            return state;
    }
}