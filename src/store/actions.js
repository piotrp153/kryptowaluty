export const GET_CRYPTO_SUCCESS = 'GET_CRYPTO_SUCCESS';
export const GET_CHART_DATA_SUCCESS = 'GET_CHART_DATA_SUCCESS';
export const GET_ERROR = 'GET_CRYPTO_ERROR';
export const CHANGE_CURRENCY = 'CHANGE_CURRENCY';
export const CHANGE_CHART_DATES = 'CHANGE_CHART_DATES';
export const CHANGE_CRYPTO_STATE = 'CHANGE_CRYPTO_STATE';

export function getCryptoSuccess(crypto) {
    return {
        type: GET_CRYPTO_SUCCESS,
        payload: crypto
    }
}

export function getChartDataSuccess(chartData) {
    return {
        type: GET_CHART_DATA_SUCCESS,
        payload: chartData
    }
}

export function getError(error) {
    return {
        type: GET_ERROR,
        payload: error
    }
}

export function changeCurrencyAction(currency) {
    return {
        type: CHANGE_CURRENCY,
        payload: currency
    }
}

export function changeChartDates(startDate, endDate) {
    return {
        type: CHANGE_CHART_DATES,
        payload: { startDate: startDate, endDate: endDate }
    }
}

export function changeCryptoState(crypto) {
    return {
        type: CHANGE_CRYPTO_STATE,
        payload: crypto
    }
}